# NOTE: This value is the key used in the query
output "full-bucket-name" {
  value       = module.bucket.bucket
  description = "Full-name of the bucket with petname suffix"
}
