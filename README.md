# Overview

This project is used to demonstrate how to query for output values in remote state using Terraform Cloud API

# Setup & Run

## Terraform Setup

1. Create a `terraform.tfvars` file with the following variable value

    ```
    project-id = "google-project-id-value-goes-here"
    ```

1. Alternatively, create an environment variable named `TF_VARS_project-id` with the value of the project ID

    ```bash
    export TF_VARS_project-id="google-project-id-value-goes-here"
    ```

1. Initialize Terraform

    ```bash
    terraform init
    ```

1. Create Resources

    ```bash
    terraform apply -auto-approve
    ```
    >NOTE: Google Project configuration may require billing to be enabled. This code assumes proper setup and configuration of Google Projects. If Google is not your thing, simply change the Terraform resources to match your cloud vendor of choice.

## Setup Environment Variables

This is a convenience step making the call to the API easier than cut-copy-paste or long CMD-line values.  While technically optional, setting these environments is recommended for easy use.

1. Add the following environment variables

    ```bash
    export TERRAFORM_WORKSPACE_ID="set-this-to-the-terraform-clould-workspace-id"
    export TERRAFORM_TOKEN_VALUE="set-this-to-the-terraform-cloud-user-token-value"
    ```

>TIP: Look into [`direnv` (docs)](https://direnv.net/) as a simple tool to set environment variables per folder, then create a `.envrc` file with the variable definitions above to provide some permanence.

## Get name of bucket

Use the bash script that demonstrates how the API is queried to obtain the state of the outputs variable `full-bucket-name` pulled from the `outputs.tf` file

1. Run script

    ```bash
    ./get-state-output.sh ${TERRAFORM_WORKSPACE_ID} ${TERRAFORM_TOKEN_VALUE} full-bucket-name
    ```
